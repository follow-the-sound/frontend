import Vue from 'vue';
import App from './App.vue';
import './index.scss';
import 'nprogress/nprogress.css'
import {router} from "./router";
import {images} from './images';
import {deferredPrompt} from "./service-worker"
import {Game} from './init';


Vue.config.productionTip = false;

new Vue({
    router,
    render: h => h(App)
}).$mount('#app');


export {
    images,
    deferredPrompt,
    Game,
}
