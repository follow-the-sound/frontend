import {Core, Kore} from "@kirinnee/core";
import {UserState} from "../../classLibrary/UserState";
import {Client} from "../../classLibrary/Client";
import Vue from "vue";

const core: Core = new Kore();
core.ExtendPrimitives();

declare var PRODUCTION: boolean;

let AllAnimals: [string, number][] = [
    ["Camel", 100],
    ["Cat", 200],
    ["Chicken", 150],
    ["Chipmunk", 1000],
    ["Cow", 5],
    ["Dog", 20],
    ["Giraffe", 900],
    ["Goat", 0],
    ["Hippo", 300],
    ["Horse", 80],
    // ["Koala", 0],
    ["Leopard", 400],
    ["Llama", 800],
    ["Pig", 40],
    ["Poodle", 600],
    ["Rabbit", 500],
    ["Ram", 60],
    ["Tiger", 700],
];


const userState = new UserState();
userState.Load();

const endpoint = PRODUCTION ? "wss://api.followthesound.work" : "ws://localhost:5001";
const client = new Client(endpoint);

const $$ = (i: number): Promise<void> => new Promise<void>(r => setTimeout(r, i));
const isMobile = (): boolean => window.innerHeight > window.innerWidth;
const isPWA = (): boolean => window.matchMedia('(display-mode: standalone)').matches;

const Game = Vue.extend({
    props: {
        state: UserState,
    }
});


export {
    $$,
    isMobile,
    isPWA,
    core,
    userState,
    client,
    Game,
    AllAnimals,
}
