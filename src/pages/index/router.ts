import Vue from 'vue'
import Router, {Route, RouteConfig} from 'vue-router'
import NotFound from "./views/NotFound.vue";
import Home from "./views/Home.vue";
import {userState} from "./init";
import Start from "./views/Start.vue";
import WaitingRoom from "./views/WaitingRoom.vue";
import JoinRoom from "./views/JoinRoom.vue";
import Game from "./views/Game.vue";
import Disconnected from "./views/Disconnected.vue";
import ResultRoom from "./views/ResultRoom.vue";
import nProgress from "nprogress";
import GamePlay from "./views/GamePlay.vue";
import Credit from "./views/Credit.vue";


Vue.use(Router);

const routes: RouteConfig[] = [
    {path: '/', name: 'home', component: Home},
    {path: '/start', name: 'start', component: Start},
    {path: '/rule', name: 'rule', component: GamePlay},
    {path: '/credits', name: 'credits', component: Credit},
    {path: '/wait', name: 'waiting', component: WaitingRoom},
    {path: '/join', name: 'join', component: JoinRoom},
    {path: '/game', name: 'game', component: Game},
    {path: '/dis', name: 'disconnect', component: Disconnected},
    {path: '/result', name: 'result', component: ResultRoom},
    {path: '*', name: '404', component: NotFound},

];
const router: Router = new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

router.afterEach(() => {
    if (nProgress.isStarted()) {
        nProgress.done();
    }
});

//Middle Ware
router.beforeEach((to: Route, from: Route, next) => {
    // nProgress.start();
    if (to.path == "/") {
        if (!userState.FirstTime) {
            router.push("/start").then();
            return
        }
        return next();
    }
    if (to.path != "/") {
        if (userState.FirstTime) {
            router.push("/").then();
            return;
        }
    }
    if (to.path == "/wait" || to.path == "/result") {
        if (!userState.InRoom) {
            router.push("/").then();
            return;
        }
    }
    if (to.path == "/game") {
        if (!userState.InGame) {
            router.push("/").then();
            return;
        }
    }
    return next();
});

export {router};
