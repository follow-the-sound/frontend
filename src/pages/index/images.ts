import {Rimage, Rimager} from "@kirinnee/rimage";
import {SortType} from "@kirinnee/core";
import {AllAnimals, core} from "./init";
import {Howl} from 'howler';


core.AssertExtend();


function LoadImages(): { [s: string]: string } {

    const arr = AllAnimals.Map(([a,]) => [a, require(`./assets/${a.toLowerCase()}.png`)]);
    const ret: { [s: string]: string } = {};
    arr.Each(([k, v]) => {
        ret[k] = v;
    });
    return ret;
}


function LoadSounds(): { [s: string]: Howl } {
    const arr = AllAnimals.Map(([a,]) => [a, require(`./sound/${a.toLowerCase()}.ogg`)]);
    const ret: { [s: string]: Howl } = {};
    arr.Each(([k, v]) => {
        ret[k] = new Howl({
            src: v, loop: true, preload: true
        });
    });
    return ret;
}

let images: any = LoadImages();
const sounds = LoadSounds();


declare var PRODUCTION: boolean;
const imageDeployConfig: any = require("../../../config/image.deploy.config.json")[0];

const config: Rimage = {
    key: imageDeployConfig.key,
    width: imageDeployConfig.width,
    sizes: imageDeployConfig.sizes
};

let rimager: Rimager = new Rimager(core, config, new SortType(), !PRODUCTION);
rimager.ExtendPrimitives();

images = rimager.RegisterImages(images);

function GenerateAnimals(): { [s: string]: { sound: Howl, image: string } } {

    const ret: { [s: string]: { sound: Howl, image: string } } = {};
    AllAnimals.Each(([e,]) => {
        ret[e] = {sound: sounds[e], image: images[e]}
    });
    return ret;
}

const animals = GenerateAnimals();

export {
    images,
    rimager,
    sounds,
    animals,
}
