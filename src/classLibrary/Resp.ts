interface Resp {
    type: string
    content: string
    sender: string
    senderName: string
    meta: any
    original: any
}

interface RoomState {
    members: Member[]

}

interface Member {
    id: string
    name: string
    score: number
    color: string
}

export {Resp, RoomState, Member}