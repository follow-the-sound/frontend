interface Req {
    type: string
    content: string
    meta: any
}

export {Req}