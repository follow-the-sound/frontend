import {Member} from "./Resp";
import {AllAnimals} from "../pages/index/init";

class UserState {
    name: string = "";
    wins: number = 0;

    id: string = "";
    roomCode: string = "";

    inGame: boolean = false;
    x: number = 0;
    y: number = 0;

    prevSelection = 'Goat';

    gameKey: string = '';

    players: Member[] = [];

    winner: string = '';
    animalType: string = 'goat';


    get FirstTime(): boolean {
        return localStorage.getItem("played") != "true"
    }

    get InRoom(): boolean {
        return this.roomCode.length === 5;
    }

    get InGame(): boolean {
        return this.inGame;
    }

    Animals(): string[] {
        return AllAnimals.Where(([, score]) => this.wins >= score)
            .Map(e => e[0]);

    }

    Load() {
        this.name = localStorage.getItem("name") ?? "";
        this.wins = (localStorage.getItem("wins") ?? "0").ToInt();
        this.prevSelection = localStorage.getItem("prev") ?? "Goat";
    }

    Save() {
        localStorage.setItem("name", this.name);
        localStorage.setItem("wins", this.wins.toString());
        localStorage.setItem("played", "true");
        localStorage.setItem("prev", this.prevSelection);
    }

}

export {UserState}