import {Resp} from "./Resp";
import {Req} from "./Req";
import {userState} from "../pages/index/init";

type CloseEventHandler = (e: CloseEvent) => any;
type MessageEventHandler = (e: Resp) => any;

class Client {

    host: string;
    conn?: WebSocket;
    closeEventHandler: { [s: string]: CloseEventHandler } = {};
    messageEventHandler: { [s: string]: MessageEventHandler } = {};


    RegisterCloseEventHandler(key: string, event: CloseEventHandler) {
        this.closeEventHandler[key] = event;
    }

    UnRegisterCloseEventHandler(key: string) {
        delete this.closeEventHandler[key]
    }

    RegisterMessageEventHandler(key: string, event: MessageEventHandler) {
        this.messageEventHandler[key] = event;
    }

    UnRegisterMessageEventHandler(key: string) {
        delete this.messageEventHandler[key]
    }

    Close() {
        userState.roomCode = '';
        userState.inGame = false;
        userState.winner = '';
        if (this.conn) {
            this.conn.close();
        }
    }


    Send(req: Req) {
        if (this.conn) {
            this.conn.send(JSON.stringify(req))
        }
    }


    async CreateRoom(name: string): Promise<[boolean, string]> {
        return this.ConnectToServer(['create', name])
    }

    async ConnectToServer(array: string[]): Promise<[boolean, string]> {
        this.Close();
        const target = this;
        if (window["WebSocket"]) {
            this.conn = new WebSocket(`${this.host}/ws`, array);
            return new Promise<[boolean, string]>((resolve) => {
                this.conn!.onerror = function (e: Event) {
                    resolve([false, "Cannot connect to server"])
                };
                this.conn!.onclose = function (e: CloseEvent) {

                    resolve([false, e.reason]);

                    for (let k in target.closeEventHandler) {
                        if (target.closeEventHandler.hasOwnProperty(k)) {
                            target.closeEventHandler[k](e);
                        }
                    }

                };
                this.conn!.onmessage = function (e: MessageEvent) {

                    const resp = JSON.parse(e.data) as Resp;
                    if (resp.type == "init") {
                        userState.id = resp.meta.id;
                        resolve([true, resp.meta.hubId]);
                    } else {
                        resolve([false, resp.content])
                    }

                    for (let k in target.messageEventHandler) {
                        if (target.messageEventHandler.hasOwnProperty(k)) {
                            target.messageEventHandler[k](resp);
                        }
                    }
                };
            });
        } else {
            return [false, "Browser not supported"];
        }
    }

    JoinRoom(name: string, code: string): Promise<[boolean, string]> {
        return this.ConnectToServer(['join', name, code]);
    }

    constructor(host: string) {
        this.host = host;
    }
}

export {Client}